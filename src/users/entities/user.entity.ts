import { IsEmail, IsString } from "class-validator";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  // ใส่กับพวก incremental

  @Column()
  email: string;
  @Column()
  password: string;
  @Column()
  fullName: string
  // roles: ('admin' | 'user')[];
  @Column()
  gender: 'male' | 'female' | 'others';

}
